<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tank;
use App\Dispenser;
use App\TankReport;
use App\DispenserReport;

class MainController extends Controller
{
    
    /**
     * Tanks controller methods go here
     */
    public function createTank(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'capacity' => 'required',
            'serial'=> 'required'
        ]);

        $tank = new Tank;
        $tank->serial_number = $request->serial;
        $tank->capacity = $request->capacity;
        $tank->name = $request->name;
        
        try{
            $tank->save();
        }catch(\Exception $e){
            $error = $e->getMessage();
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"Tank Created",
                "data" => $tank
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function deleteTank($id){
        $tank = Tank::findOrFail($id);

        try {
            $tank->delete();
        } catch (\Exception $e) {
            $error = $e->getMessage();   
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"Tank Created",
                "data" => $tank
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function editTank(Request $request, $id){   
        
        $this->validate($request,[
            'name' => 'required',
            'capacity' => 'required',
            'serial'=> 'required'
        ]);
        
        $tank = Tank::findOrFail($id);

        $tank->serial_number = $request->serial;
        $tank->capacity = $request->capacity;
        $tank->name = $request->name;
        
        try{
            $tank->save();
        }catch(\Exception $e){
            $error = $e->getMessage();
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"Tank Updated",
                "data" => $tank
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }
    public function getTanks(){
        return response()->json([
            "message" => "Tanks found",
            "data" => Tank::class
        ],200);
    }

    /**
     * Dispensers Controller methods
     */

    public function createDispenser(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'capacity' => 'required',
            'serial'=> 'required'
        ]);
        
        $dispenser = new Dispenser;
        $dispenser->serial_number = $request->serial;
        $dispenser->capacity = $request->capacity;
        $dispenser->name = $request->name;
        
        try{
            $dispenser->save();
        }catch(\Exception $e){
            $error = $e->getMessage();
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"dispenser Created",
                "data" => $dispenser
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function deleteDispenser($id){
        $dispenser = Dispenser::findOrFail($id);

        try {
            $dispenser->delete();
        } catch (\Exception $e) {
            $error = $e->getMessage();   
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"dispenser Created",
                "data" => $dispenser
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function editDispenser(Request $request, $id){
        $this->validate($request,[
            'name' => 'required',
            'capacity' => 'required',
            'serial'=> 'required'
        ]);
        
        $dispenser = Dispenser::findOrFail($id);
        $dispenser->serial_number = $request->serial;
        $dispenser->capacity = $request->capacity;
        $dispenser->name = $request->name;
        
        try{
            $dispenser->save();
        }catch(\Exception $e){
            $error = $e->getMessage();
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"dispenser updated",
                "data" => $dispenser
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function getDispensers(){
        return response()->json([
            "message" => "dispensers found",
            "data" => Dispenser::class
        ],200); 
    }

    /**
     * Tanks Report methods
     */

    public function uploadTankReport(Request $request){
        $this->validate($request,[
            'date' => 'required|date|after:today',
            'product' => 'required',
            'tank' => 'required'
        ]);

        $report = new TankReport;
        $report->date = $request->date;
        $report->product_left = $request->product;

        try{
            $report->save();
        }catch(\Exception $e){
            $error = $e->getMessage();
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"report created",
                "data" => $report
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function getTankReports(){
        return response()->json([
            "message" => "reports found",
            "data" => TankReport::class
        ],200); 
    }
    /**
     * Dispensers Reports Methods
     */

    public function uploadDispenserReport(Request $request){
        $this->validate($request,[
            'date' => 'required|date|after:today',
            'product' => 'required',
            'dispenser_id' => 'required'
        ]);

        $report = new DispenserReport;
        $report->dispenser_id = $request->dispenser_id;
        $report->date = $request->date;
        $report->product_sold = $request->product;

        try{
            $report->save();
        }catch(\Exception $e){
            $error = $e->getMessage();
        }

        if(!isset($error)){
            return response()->json([
                "message"=>"report created",
                "data" => $report
            ],200);
        }else{
            return response()->json([
                "message" => $error
            ],500);
        }
    }

    public function getDispenserReports(){
        return response()->json([
            "message" => "reports found",
            "data" => DispenserReport::class
        ],200); 
    }

}
